import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class NoeudBroadcast extends Node {
    private Boolean received = false;
    private static int COMPTE = 0;

    public void onStart() {

    }

    public void onSelection() {
        System.out.println("Vous avez sélectionné le noeud numéro " + getID() + "!");
        
        Message m = new Message("Broadcast");
        this.received = true;
        setColor(Color.WHITE);
        sendAll(m);
    }

    // Cette méthode est appelée lors de la réception d’un message
    public void onMessage(Message m) {
        if(!this.received) {
            System.out.println("Le noeud " + getID() + " a reçu le message suivant du noeud "
                    + m.getSender() + ": \"" + m.getContent() + "\"");
            setColor(Color.RED);
            this.received = true;
            sendAll(m);

            if(allReceived()) {
                System.out.println("Tous les messages ont été reçus. Nombre de messages : " + COMPTE);
            }
        }
    }

    @Override
    public void sendAll(Message m) {
        COMPTE += getNeighbors().size();
        System.out.println("Envoi de " + getNeighbors().size() + " messages. Total : " + COMPTE);
        super.sendAll(m);
    }

    private Boolean allReceived() {
        for(Node n : getTopology().getNodes()) {
            if(((NoeudBroadcast)n).received == false) {
                return false;
            }
        }

        return true;
    }
}
