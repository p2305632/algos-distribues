import java.util.ArrayList;
import java.util.List;

import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class ArbreCouvrant extends Node {
    public static int COMPTE_ARBRE = 0;
    public static int COMPTE_BCAST = 0;
    private Node father;
    private boolean root = false;
    List<Node> receivedFrom;
    List<Node> children;
    boolean firstReceived = true;

    public void onStart() {
        father = null;
        receivedFrom = new ArrayList<>();
        children = new ArrayList<>();
    }

    public void onSelection() {
        System.out.println("Vous avez sélectionné le noeud numéro " + getID() + "!");
        root = true;
        
        Message m = new Message("Join");
        setColor(Color.YELLOW);
        sendAll(m);
        COMPTE_ARBRE += getNeighbors().size();
    }

    // Cette méthode est appelée lors de la réception d’un message
    public void onMessage(Message m) {
        if (m.getContent().equals("Join")) {
            if (firstReceived) {
                firstReceived = false;

                father = m.getSender();

                receivedFrom.add(m.getSender());

                if (!receivedFromAll()) {
                    sendAllExcept(new Message("Join"), m.getSender());
                    COMPTE_ARBRE += getNeighbors().size() - 1;
                }
            } else {
                send(m.getSender(), new Message("Backno"));
                COMPTE_ARBRE++;
            }

            if (receivedFromAll()) {
                send(father, new Message("Back"));
                COMPTE_ARBRE++;
            }

        } else if (((String)m.getContent()).startsWith("Back")) {
            if (m.getContent().equals("Back")) {
                children.add(m.getSender());

                getCommonLinkWith(m.getSender()).setColor(Color.YELLOW);
                m.getSender().setColor(Color.YELLOW);
            }
            receivedFrom.add(m.getSender());

            if (father != null && receivedFromAll()) {
                send(father, new Message("Back"));
                COMPTE_ARBRE++;
            }
        }

        if (root && receivedFromAll()) {
            setColor(Color.RED);

            System.out.println("Messages pour création de l'arbre : " + COMPTE_ARBRE);

            sendAllChildren(new Message("BCAST"));
            COMPTE_BCAST += children.size();
        }

        if (m.getContent().equals("BCAST")) {
            setColor(Color.RED);
            sendAllChildren(new Message("BCAST"));
            COMPTE_BCAST += children.size();
            System.out.println("Réception d'un BCAST. Compte : " + COMPTE_BCAST);
        }
    }

    @Override
    public void sendAll(Message m) {
        super.sendAll(m);
    }

    private void sendAllExcept(Message m, Node excNode) {
        for(Node n : getNeighbors()) {
            if (!n.equals(excNode))
                send(n, m);
        }
    }

    private boolean receivedFromAll() {
        for(Node n : getNeighbors()) {
            if (!receivedFrom.contains(n)) {
                return false;
            }
        }

        return true;
    }

    private void sendAllChildren(Message m) {
        for (Node n : children) {
            send(n, m);
        }
    }
}
