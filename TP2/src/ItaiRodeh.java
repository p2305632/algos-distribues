import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

/**
 * Résultats de la batterie de tests avec environ 20% de noeuds candidats :
 * 
 * Messages cumulés : 2717
 * Nombre minimum de messages : 20
 * Nombre maximum de messages : 49
 * Fin des exécutions.
 * Nombre moyen de messages par exécution: 27.17
 */

public class ItaiRodeh extends Node {
    private Etat etat = null;
    private Integer leader = null;
    private int phaseActuelle;

    public static int CANDIDATS_INITIAUX = 0;
    public static int COMPTE_MESSAGES = 0;
    public static int COMPTE_EXECUTIONS = 0;
    public static int EXECUTIONS_MAX = 100;
    public static int MIN_MESSAGES = Integer.MAX_VALUE;
    public static int MAX_MESSAGES = Integer.MIN_VALUE;
    public static int TOTAL_MESSAGES = 0;

    public void onStart() {
        phaseActuelle = 1;
        leader = this.getID();

        if (this.getID() == 0 || Math.random() < 0.2) {
            etat = Etat.CANDIDAT;
            CANDIDATS_INITIAUX++;
        } else {
            etat = Etat.NON_CANDIDAT;
        }



        if (this.etat == Etat.CANDIDAT) {
            candidater();
        }
    }

    // Cette méthode est appelée lors de la réception d’un message
    public void onMessage(Message m) {
        ContenuItaiRodeh content = (ContenuItaiRodeh)m.getContent();
        int nbNoeuds = getTopology().getNodes().size();
        

        if (content.termine) {
            if (content.nombreTransmissions == nbNoeuds) {
                onEnd();
            } else {
                leader = content.id;

                sendMsgToSucc(content.id, content.numeroPhase, content.nombreTransmissions + 1, content.unique, content.termine);
            }

            return;
        }

        if (etat != Etat.CANDIDAT) {
            sendMsgToSucc(content.id, content.numeroPhase, content.nombreTransmissions + 1, content.unique, content.termine);

            return;
        }
         
        if (content.nombreTransmissions == nbNoeuds) { // Recoit son propre message
            if (content.unique) { // Id unique
                etat = Etat.ELU;
                sendMsgToSucc(this.getID(), phaseActuelle, 1, true, true);
            } else {
                phaseActuelle++;
                candidater();
            }
        } else if (content.id == this.getID() && content.numeroPhase == phaseActuelle) {
            content.unique = false;

            sendMsgToSucc(content.id, content.numeroPhase, content.nombreTransmissions + 1, content.unique, content.termine);
        } else if (content.numeroPhase > phaseActuelle || (content.numeroPhase == phaseActuelle && content.id > this.getID())) {
            this.etat = Etat.PERDU;

            sendMsgToSucc(content.id, content.numeroPhase, content.nombreTransmissions + 1, content.unique, content.termine);
        }
    }

    private void sendMsgToSucc(Message message) {
        send(getOutLinks().get(0).destination, message);
    }

    private void sendMsgToSucc(int id, int numeroPhase, int nombreTransmissions, boolean unique, boolean termine) {
        Message msg = new Message(
            new ContenuItaiRodeh(
                id,
                numeroPhase,
                nombreTransmissions,
                unique,
                termine
            )
        );

        sendMsgToSucc(msg);
    }

    private void candidater() {
        Random rand = new Random();
        int id = rand.nextInt() % 21;

        this.setID(id);

        sendMsgToSucc(id, phaseActuelle, 1, true, false);
    }

    @Override
    public void send(Node dest, Message message) {
        super.send(dest, message);

        COMPTE_MESSAGES++;
    }

    private void onEnd() {
        //System.out.println(this.getID() + " a reçu son propre message d'élection. Fin de l'algorithme.");
        COMPTE_EXECUTIONS++;
        TOTAL_MESSAGES += COMPTE_MESSAGES;
        
        if (COMPTE_MESSAGES < MIN_MESSAGES) {
            MIN_MESSAGES = COMPTE_MESSAGES;
        }
        
        if (COMPTE_MESSAGES > MAX_MESSAGES) {
            MAX_MESSAGES = COMPTE_MESSAGES;
        }

        System.out.println("Fin de l'exécution " + COMPTE_EXECUTIONS + ". Nombre de messages envoyés: " + COMPTE_MESSAGES + " pour " + CANDIDATS_INITIAUX + " candidats initiaux.");
        System.out.println("Messages cumulés : " + TOTAL_MESSAGES);
        System.out.println("Nombre minimum de messages : " + MIN_MESSAGES);
        System.out.println("Nombre maximum de messages : " + MAX_MESSAGES);

        COMPTE_MESSAGES = 0;
        CANDIDATS_INITIAUX = 0;

        if (COMPTE_EXECUTIONS < EXECUTIONS_MAX) {
            ArrayList<Integer> IDs = new ArrayList<>();

            for (int i = 0; i < getTopology().getNodes().size(); i++)
            {
                IDs.add(i);
            }

            Collections.shuffle(IDs);

            for (int i = 0; i < getTopology().getNodes().size(); i++)
            {
                getTopology().getNodes().get(i).setID(IDs.get(i));
            }

            getTopology().restart();
        } else {
            System.out.println("Fin des exécutions.");
            System.out.println("Nombre moyen de messages par exécution: " + (double)TOTAL_MESSAGES / (double)EXECUTIONS_MAX);
        }
    }
}
