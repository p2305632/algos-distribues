import io.jbotsim.core.Node;
import io.jbotsim.core.Topology;

import java.util.ArrayList;
import java.util.Collections;

import io.jbotsim.core.Link;
import io.jbotsim.core.Link.Orientation;
import io.jbotsim.ui.JViewer;

public class TP2Main {
    public static final double TAILLE_FENETRE = 500;
    public static final int TAILLE_ANNEAU = 10;

    public static void main(String[] args) { // On déclare le programme principal
        ItaiRodeh();
    }

    public static void ChangRoberts() {
        Topology tp = new Topology((int)TAILLE_FENETRE, (int)TAILLE_FENETRE);
        tp.disableWireless();
        //tp.setTimeUnit(1000);

        Node last = null;

        ArrayList<Integer> IDs = new ArrayList<>();

        for (int i = 0; i < TAILLE_ANNEAU; i++)
        {
            IDs.add(i);
        }

        Collections.shuffle(IDs);

        for (int i = 0; i < TAILLE_ANNEAU; i++)
        {
            ChangRoberts n = new ChangRoberts();

            tp.addNode(
                TAILLE_FENETRE * (0.5 + Math.cos(2.0 * Math.PI * (double)i / (double)TAILLE_ANNEAU) * 0.4),
                TAILLE_FENETRE * (0.5 - Math.sin(2.0 * Math.PI * (double)i / (double)TAILLE_ANNEAU) * 0.4),
                n
            );

            n.setID(IDs.get(i));

            if (last != null)
            {
                tp.addLink(new Link(last, n, Orientation.DIRECTED));
            }

            last = n;
        }

        tp.addLink(new Link(last, tp.findNodeById(IDs.get(0)), Orientation.DIRECTED));
        
        //new JViewer(tp);
        tp.start();
    }

    public static void ItaiRodeh() {
        Topology tp = new Topology((int)TAILLE_FENETRE, (int)TAILLE_FENETRE);
        tp.disableWireless();
        //tp.setTimeUnit(1000);

        Node last = null;

        for (int i = 0; i < TAILLE_ANNEAU; i++)
        {
            ItaiRodeh n = new ItaiRodeh();

            tp.addNode(
                TAILLE_FENETRE * (0.5 + Math.cos(2.0 * Math.PI * (double)i / (double)TAILLE_ANNEAU) * 0.4),
                TAILLE_FENETRE * (0.5 - Math.sin(2.0 * Math.PI * (double)i / (double)TAILLE_ANNEAU) * 0.4),
                n
            );

            if (last != null)
            {
                tp.addLink(new Link(last, n, Orientation.DIRECTED));
            }

            last = n;
        }

        tp.addLink(new Link(last, tp.getNodes().get(0), Orientation.DIRECTED));
        
        //new JViewer(tp);
        tp.start();
    }
}