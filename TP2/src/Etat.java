public enum Etat {
    NON_CANDIDAT,
    CANDIDAT,
    ELU,
    PERDU
}