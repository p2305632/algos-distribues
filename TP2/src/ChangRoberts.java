import java.util.ArrayList;
import java.util.Collections;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

/**
 * Hypothèses de l'agorithme de C-R:
 * Topologie en anneau unidirectionnel,
 * Chaque noeud a un Id unique, et sait que les Ids sont uniques,
 * Chaque noeud connait son voisin,
 * Le nombre de noeuds du système est inconnu de chaque noeud.
 */

/**
 * Résultats de la batterie de tests avec tous les noeuds candidats :
 * 
 * Messages cumulés : 3943
 * Nombre minimum de messages : 33
 * Nombre maximum de messages : 51
 * Fin des exécutions.
 * Nombre moyen de messages par exécution: 39.43
 */

/**
 * Résultats de la batterie de tests avec environ 20% de noeuds candidats :
 * 
 * Messages cumulés : 3159
 * Nombre minimum de messages : 21
 * Nombre maximum de messages : 43
 * Fin des exécutions.
 * Nombre moyen de messages par exécution: 31.59
 */

public class ChangRoberts extends Node {
    private Etat etat = null;
    private Integer leader = null;
    private Integer succ = null;

    public static int COMPTE_MESSAGES = 0;
    public static int COMPTE_EXECUTIONS = 0;
    public static int EXECUTIONS_MAX = 100;
    public static int MIN_MESSAGES = Integer.MAX_VALUE;
    public static int MAX_MESSAGES = Integer.MIN_VALUE;
    public static int TOTAL_MESSAGES = 0;

    public void onStart() {
        succ = getOutLinks().get(0).destination.getID();
        leader = this.getID();

        if (this.getID() == 0 || Math.random() < 0.2) {
            etat = Etat.CANDIDAT;
        } else {
            etat = Etat.NON_CANDIDAT;
        }

        //System.out.println("Successeur de " + getID() + ": " + succ.getID()); 

        if (this.etat == Etat.CANDIDAT) {
            candidater();
        }
    }

    public void onSelection() {
        
    }

    // Cette méthode est appelée lors de la réception d’un message
    public void onMessage(Message m) {
        ContenuMessage content = (ContenuMessage)m.getContent();

        if (content.typeMessage == TypeMessage.CANDIDATURE) {
            if (content.id > this.leader) {
                this.leader = content.id;

                sendMsgToSucc(m);
            }

            if (content.id < this.leader) {
                candidater();
            }

            if (content.id == this.getID()) {
                //System.out.println(this.getID() + " a reçu son propre ID en candidature. Donc " + this.getID() + " est élu leader."); 
                sendMsgToSucc(TypeMessage.ELECTION, this.getID());
            }
        }

        if (content.typeMessage == TypeMessage.ELECTION) {
            if (content.id == this.getID()) {
                onEnd();
            } else {
                //System.out.println(this.getID() + " a reçu un message d'élection. Son nouveau leader est maintenant " + content.id); 
                this.leader = content.id;
                sendMsgToSucc(m);
            }
        }

    }

    private void sendMsgToSucc(Message message) {
        send(getTopology().findNodeById(succ), message);
    }

    private void sendMsgToSucc(TypeMessage type, Integer id) {
        Message msg = new Message(
            new ContenuMessage(
                type,
                id
            )
        );

        sendMsgToSucc(msg);
    }

    private void candidater() {
        sendMsgToSucc(TypeMessage.CANDIDATURE, this.getID());
    }

    @Override
    public void send(Node dest, Message message) {
        super.send(dest, message);

        COMPTE_MESSAGES++;
    }

    private void onEnd() {
        //System.out.println(this.getID() + " a reçu son propre message d'élection. Fin de l'algorithme.");
        COMPTE_EXECUTIONS++;
        TOTAL_MESSAGES += COMPTE_MESSAGES;
        
        if (COMPTE_MESSAGES < MIN_MESSAGES) {
            MIN_MESSAGES = COMPTE_MESSAGES;
        }
        
        if (COMPTE_MESSAGES > MAX_MESSAGES) {
            MAX_MESSAGES = COMPTE_MESSAGES;
        }

        int candidatsInitiaux = 0;

        for (int i = 0; i < getTopology().getNodes().size(); i++) {
            if (((ChangRoberts)getTopology().getNodes().get(i)).etat == Etat.CANDIDAT) {
                candidatsInitiaux++;
            }
        }

        System.out.println("Fin de l'exécution " + COMPTE_EXECUTIONS + ". Nombre de messages envoyés: " + COMPTE_MESSAGES + " pour " + candidatsInitiaux + " candidats initiaux.");
        System.out.println("Messages cumulés : " + TOTAL_MESSAGES);
        System.out.println("Nombre minimum de messages : " + MIN_MESSAGES);
        System.out.println("Nombre maximum de messages : " + MAX_MESSAGES);

        COMPTE_MESSAGES = 0;

        if (COMPTE_EXECUTIONS < EXECUTIONS_MAX) {
            ArrayList<Integer> IDs = new ArrayList<>();

            for (int i = 0; i < getTopology().getNodes().size(); i++)
            {
                IDs.add(i);
            }

            Collections.shuffle(IDs);

            for (int i = 0; i < getTopology().getNodes().size(); i++)
            {
                getTopology().getNodes().get(i).setID(IDs.get(i));
            }

            getTopology().restart();
        } else {
            System.out.println("Fin des exécutions.");
            System.out.println("Nombre moyen de messages par exécution: " + (double)TOTAL_MESSAGES / (double)EXECUTIONS_MAX);
        }
    }
}
