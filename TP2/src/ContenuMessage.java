public class ContenuMessage {
    public TypeMessage typeMessage;
    public Integer id;

    public ContenuMessage(TypeMessage type, Integer id)
    {
        this.typeMessage = type;
        this.id = id;
    }
}
