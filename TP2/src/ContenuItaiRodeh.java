public class ContenuItaiRodeh {
    public Integer id;
    public Integer numeroPhase;
    public Integer nombreTransmissions;
    public boolean unique;
    public boolean termine;

    public ContenuItaiRodeh(int id, int numeroPhase, int nombreTransmissions, boolean unique, boolean termine) {
        this.id = id;
        this.numeroPhase = numeroPhase;
        this.nombreTransmissions = nombreTransmissions;
        this.unique = unique;
        this.termine = termine;
    }
}
